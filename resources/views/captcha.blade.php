<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form action="/test/checkCaptcha" method="post">
    <input type="text" name="captcha" placeholder="验证码"><br>
    <img src="{{captcha_src()}}" alt="">
    {{csrf_field()}}
    <br>
    <button>提交</button>
</form>

@if(count($errors)>0)
<div class="alert alert-danger">
<ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
</ul>
</div>
@endif
</body>
</html>