<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/public/static/bootstrap/css/bootstrap.min.css">
</head>
<body>
    <table border="1">
        <thead>
            <tr>
            <th>id</th>
            <th>用户名</th>
            <th>年龄</th>
            <th>邮箱</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $val)
            <tr>
                <th>{{ $val->id }}</th>
                <th>{{ $val->name }}</th>
                <th>{{ $val->age }}</th>
                <th>{{ $val->email }}</th>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$data->links()}}
</body>
</html>