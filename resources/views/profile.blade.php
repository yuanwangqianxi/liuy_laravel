<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="{{route('store')}}" method="post">
        姓名： <input type="text" name="name" id=""><br>
        邮件： <input type="text" name="email" id=""><br>
        年龄： <input type="text" name="age" id=""><br>
        爱好： <input type="checkbox" name="hobby" id="">足球<br>
               <input type="checkbox" name="hobby" id="">篮球<br>
               <input type="checkbox" name="hobby" id="">排球 <br>
               {{csrf_field()}}
               <input type="submit" value="保存">
    </form>
    @if($errors->any())
    @if ($errors->has('name'))
        姓名格式错误！
    @endif
    @foreach ($errors->get('name') as $message) 
        {{ $message }}
    @endforeach
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
</body>
</html>