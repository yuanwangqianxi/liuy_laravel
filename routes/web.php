<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/hello', function () {
    return 'hello';
});
Route::match(['get', 'post'], 'test1', function () {
    return '通过match()匹配';
});

Route::any('test2', function () {
    return '通过any()匹配';
});
Route::get('find/{id}', function ($id) {
    return '输入的ID为' . $id;
});
//
Route::any('find2/{id?}', function ($id = 0) {
    return '输入的ID为' . $id;
});
//重定向
Route::redirect('/hello','/');
//路由别名
Route::get('/hello/123/452', function () {
    return '123';
})->name(123);
//路由分组
Route::group(['prefix'=>'admin'], function () {
    Route::get('/hello', function () {
        return '这是/admin/hello';
    });
    Route::get('/login', function () {
        return '/admin/login';
    });
    Route::get('/index', function () {
        return '/admin/index';
    });
});
//控制器路由
Route::get('admin/text1','MyController@text1');
Route::get('admin/text2','Admin\TestController@text2');
//Route::get('test/input','TestController@input');
Route::get('test/input/{name}','TestController@input');

// 通过路由参数接受用户输入
Route::get('test1/input/{name}','Admin\MyController@input');

//  视图路由
// Route::get('shop/index','Shop\ShopController@shop');
//模板
Route::get('mo/index','Shop\ShopController@show');

// laravel 防御CSRF攻击
Route::get('text/index','TestController@form')->middleware('test');
Route::post('text/transfer','TestController@transfer')->name('trans');

// 验证规则路由

Route::get('text/profile','TestController@profile');
Route::post('text/store','TestController@store')->name('store');


Route::get('test/testSession','TestController@testSession');


Route::group(['middleware'=>['test']], function () {
    Route::get('/', function () {
        return  view('welcome');
    });
});

// 登录路由
Route::get('user/login','UserController@login');
Route::post('user/check','UserController@check')->name('check');

Route::get('user/index','UserController@index')->middleware('user');

Route::get('test/database','TestController@database');
Route::get('test/hahahaha','TestController@hahahaha');
Route::get('test/dataNew','TestController@dataNew');
Route::get('test/members','TestController@members');

Route::get('test/avatar','TestController@avatar');
Route::post('test/up','TestController@up');

Route::get('test/user','TestController@user');

Route::get('test/captcha','TestController@captcha');
Route::post('test/checkCaptcha','TestController@checkCaptcha');

Route::get('test/cache','TestController@cache');
Route::get('test/testArr','TestController@testArr');
Route::get('test/testStr','TestController@testStr');
Route::get('test/testURL','TestController@testURL');

Route::get('test/xmldata','TestController@xmldata');
Route::get('test/xml','TestController@xml');

Route::get('test/jsondata','TestController@jsondata');
Route::get('test/json','TestController@json');



