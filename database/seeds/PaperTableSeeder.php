<?php

use Illuminate\Database\Seeder;

class PaperTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('paper')->insert([
            [
                'paper_name' => 'PHP期末复习题',
                'total_score' => 100,
                'start_time'=> time() + 9999,
                'duration'=>120,
                'status'=>1
            ],
        ]);
    }
}
