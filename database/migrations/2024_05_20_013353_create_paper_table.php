<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paper', function (Blueprint $table) {
            $table->increments('id')->comment('主键');
            $table->string('paper_name', 100)->comment('试卷名称')->unique();
            $table->tinyInteger('total_score')->default(0)->comment('试卷总分');
            $table->integer('start_time')->comment('考试开始时间');
            $table->tinyInteger('duration')->comment('考试时长');
            $table->tinyInteger('status')->default(1)->comment('状态');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paper');
    }
}
