<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //登录方法
    public function login()
    {
        return view('user.login');
    }
    // 登录验证
    public function check(Request $request)
    {
        // 传入数据
        $name=$request->input('name');
        $password=$request->input('password');
        // 验证规则
        $rule=[
            'name'=>'required',
            'password'=>'required'
        ];
        $message=[
            'name.required'=>'用户名不能为空',
            'password.required'=>'密码不能为空',
        ];
        $validator=Validator::make($request->all(),$rule,$message);
        // 输出验证结果
        if($validator->fails()){
            foreach ($validator->getMessageBag()->toArray() as $v){
                $msg=$v[0];
            }
            return $msg;
        }
        // 判断用户名和密码是否正确
        if ($name!='admin'|| $password!='123456') {
            return '用户密码正确';
        }
        session(['users'=>['id'=>1,'name'=>'admin']]);
        return '登录成功!';
    }
    public function index()
    {
        return view('user.index');
    }
}
