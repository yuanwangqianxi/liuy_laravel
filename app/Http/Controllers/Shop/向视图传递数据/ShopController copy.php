<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{
    //
    public function shop()
    {
        // 向视图传递数据
        // 方法一
        // $data=[
        //     'content'=>'文本内容'
        // ];
        // return view('shop.index.shop',$data);
        // 方法二
        // return view('shop.index.shop')->with('content','这是我');
        //view('shop') === /resources/views/shop.blade.php

        // // 视图数据的处理
        // // 1.特殊字符转义
        // return view('shop.index.shop')->with('content','<b>加粗文字</b>');
        // <!--shop中的要删掉 {!!$content!!} -->
        // 2.通过函数对数据进行处理
        // return view('shop.index.shop')->with('time',time());


    }
}
