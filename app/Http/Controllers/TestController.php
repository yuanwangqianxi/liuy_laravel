<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Member;
use App\Models\Article;
use Cache;
use Str;
use URL;

use xmldata;
use xml;
use jsondata;

class TestController extends Controller
{
    //
    public function input(Request $request)
    {
        // $name=$request->input('name');
        $name=$request->name;

    return 'name的值为' . $name;
    }
    public function form()
    {
        return view('form');
    }
    public function transfer(){
        return "转账成功";
    }
    public function profile()
    {
        //用于显示用户输入的表单
        // dump(session()->all()); //信息报错块
        return view('profile');
    }
    public function store(Request $request)
    {
        //用于接收表单提交内容
        //添加验证规则

        $validatedDate=$request->validate([
            'name'=> 'required|string|bail|max:10',
            'email'=> 'required|email',
            'age'=> 'required|integer',
            'hobby'=>'required'
        ], [
            'name.required' => '姓名不能为空!',
            'name.string' => '姓名必须是字符格式!',
            'email.required' => '邮箱地址不能为空!',
            'email.email' =>'邮件格式不正确',
            'age.required'=>'年龄不能为空',
            'age.integer'=>'年龄必须是数字',
            'hobby.required'=>'爱好不能为空!'
        ]);
    }
    public function testSession()
    {
        session(['name'=>'张三','age'=>25]);
        dump(session('name'));
        // 取值不存在时
        dump(session('age',0));
        dump(session()->all()); 	// 获取所有Session
        // dump(session()->forget('name')); // 删除名称为name的Session
        dump(session()->has('name')); 	// 判断Session是否存在
        session()->flush(); 		// 删除全部Session

    }

    public function database()
    {
        // $data = DB::table('member')->get();
        // foreach ($data as $v) {
        //     dump($v->id . '-' . $v->name);
        // }
  }
    public function hahahaha()
    {
        //  $data = DB::table('member')->get();
        // foreach ($data as $v) {
        //     dump($v->id . '-' . $v->name);
        // }
        // $data = [
        //     'name' => 'and',
        //     'age' => '22',
        //     'email' => 'tom@laravel.test'
        //     ];
        // insert()方法
        // dump(DB::table('member')->insert($data));
        // // insertGetId()方法
        // dump(DB::table('member')->insertGetId($data));
  

        // $data = [
        //     ['name' => 'tom', 'age' => '23', 'email' => 'tom@laravel.test'],
        //     ['name' => 'jim', 'age' => '24', 'email' => 'jim@laravel.test'],
        //     ['name' => 'tim', 'age' => '25', 'email' => 'tim@laravel.test'],
        // ];
        // // insert()方法
        // dump(DB::table('member')->insert($data));

  // 将表中所有记录的name字段的值都改为tom
//         $data = ['name' => 'tom'];
//         dump(DB::table('member')->update($data));
// // 将表中所有记录的age字段的值都加1
//         dump(DB::table('member')->increment('age'));
// // 将表中所有记录的age字段的值都减1
//         dump(DB::table('member')->decrement('age'));
// // 将表中所有记录的age字段的值都加5
//         dump(DB::table('member')->increment('age', 5));
// //将表中所有记录的age字段的值都减5
//         dump(DB::table('member')->decrement('age', 5));
//      $data = [
//             // 添加单条数据
//             'name' => 'adler',
//             'age' => '66',
//             'email' => 'adler@laravel.test'
//         ];
//         // 参数形式1：where(字段名, 运算符, 字段值)
//         DB::table('member')->where('id', '=', '1')->update($data);
//         // 参数形式2：where(字段名, 字段值)，使用“=”运算符
//         DB::table('member')->where('id', '1')->update($data);
//         // 参数形式3：where([字段名 => 字段值])，使用“=”运算符，支持多个字段，AND关系
//         DB::table('member')->where(['id' => 1])->update($data);
        // where()表示AND，即 “WHERE id=1 AND name='tom'”
        // DB::table('member')->where(['id' => 6])->where(['name' => 'tom'])->update($data);
        // orWhere()表示OR，即“WHERE id=1 OR name='tom'”
        // DB::table('member')->where(['id' => 1])->orWhere(['name' => 'tom'])->update($data);

       
}
        public function dataNew()
    {
        // $data = DB::table('member')->get();
        // 指定查询条件
        // $data=DB::table('member')->where('id', '<', 3)->get();
        // foreach ($data as $v) {
        //     echo $v->id . '-' . $v->name . '<br>';
        // }
        // 查询id为1的记录
        // $data = DB::table('member')->where('id', '1')->first();
        // // 输出id字段的值
        // dump($data->name);
        // 获取name和email两个字段，返回多条记录
        // $data = DB::table('member')->get(['name', 'email']);
        // dump($data);
        // $data = DB::table('member')->first(['name', 'email']);
        // dump($data);
        // $data = DB::table('member')->select(['name', 'email'])->get();
        // dump($data);
        // $data = DB::table('member')->select('name as username')->get();
        // dump($data);
        // $data = DB::table('member')->select(DB::raw('name,age'))->get();
        // dump($data);
        // 查询id为1的记录，返回name字段的值
        // $name = DB::table('member')->where('id', '1')->value('name');
        // // 输出结果
        // dump($name);

        // $data = DB::table('member')->orderBy('age', 'desc')->get();
        // dump($data);
        // $data = DB::table('member')->limit(3)->offset(2)->get();
        // dump($data);

        // $data = DB::table('article AS t1')->select(
        //     't1.id',
        //     't1.article_name AS article_name',
        //     't2.author_name AS author_name'
        // )->leftjoin(
        //         'author AS t2',
        //         't1.author_id',
        //         '=',
        //         't2.id'
        //     )->get();
        // foreach ($data as $v) {
        //     echo $v->id . '-' . $v->article_name . '-' . $v->author_name;
        // }
}
    public function members()
    {
        // Member::get();
        // // 2实列化模型
        // $member = new Member();
        // $member->get();

        // save
        // $member = new Member();
        // $member->name = 'save';
        // $member->age= '20';
        // $member->email='save@laravel.test';
        // dump($member->save());
        // dump($member->id);
        // fill
        // $data = ['name' => 'fill', 'age' => '20', 'email'=>'fill@laravel.test'];
        // $member = new Member();
        // $member->fill($data);
        // $member->save();
        // create
        // $data=['name'=>'tom','age'=>'20'];
        // $member=Member::create($data);
        // $member->save();
        // 模型查询
        // $member = Member::find(4);
        // dump($member->name);
        // 返回name和age字段
        // $member = Member::where('name', 'tom')->select('name','age')->find(1);
        // dump($member);
        // 主键wei123
        // $members = Member::find([1,2,3]);
        // dump($members);
        // get()
        // $members = Member::where('id', '1')->get();
        // dump(get_class($members[0]));
        // DBdeget()
        // $members = DB::table('member')->where('id',1)->get();
        // dump(get_class($members[0]));
        // all()
        // $members = Member::all();
        // dump($members);
        // $members = Member::all(['name','age']);
        // dump($members);
        // 模型修改数据
        // $member=Member::find(4);
        // if($member){
        //     $member->name='test';
        //     $member->email='test@laravel.net';
        //     $member->save();
        //     dump('修改成功');
        // }else{
        //     dump('修改失败');
        // }
        // 直接改
        // Member::where('id',4)->update(['name'=>'test', 'age' => 30]);
        // 删除数据
        // $member=Member::find(4);
        // if($member) {
        //     $member->delete();
        // }else{
        //     dump('删除失败：记录不存在');
        // }
        // 直接删除
        // $data = Member::where('id',5)->delete();
        // dump($data);
        // 关联模型使用
        // 一对一
        // $data = \App\Article::all();
        // foreach ($data as $key => $value) {
        //     echo '文章id: ' . $value->id . '<br>';
        //     echo '文章名称: ' . $value->article_name . '<br>';
        //     echo '作者名称: ' . $value->author->author_name . '<br>';
        // }
        // 一对多
        // $data = \App\Author::all();
        // foreach ($data as $key => $value){
        //     echo '作者名称' . $value->author_name . '<br>';
        //     foreach ($value->article as $k => $v) {
        //         echo $v->article_name . '<br>';
        //     }
        // }
        // 多对一
        //  $data = \App\Article::all();
        // foreach ($data as $key => $value) {
        //     echo '文章id: ' . $value->id . '<br>';
        //     echo '文章名称: ' . $value->article_name . '<br>';
        //     echo '作者名称: ' . $value->author->author_name . '<br>';
        // }
        // 多对多
        //   $data = \App\Article::all();
        //   foreach ($data as $key => $value) {
        //     echo '文章名称: ' . $value->article_name . '<br>';
        //     echo '关键词: ' ;
        //    foreach ($value->keyword as $k => $v){
        //     echo $v->keyword . '' ;
        //    }
        //     echo '<hr>';
        // }
    //     $data = \App\Keyword::all();
    //     foreach ($data as $key => $value) {
    //       echo '关键词: ' . $value->keyword . '<br>';
    //       echo '相关文章: ' ;
    //       foreach ($value->article as $k => $v){
    //       echo $v->article_name . '' ;
    //      }
    //       echo '<hr>';
    //   }
    }
    public function avatar()
    {
        return view('avatar');
    }
    public function up(Request $request)
    {
        if($request->hasFile('avatar')){
            $avatar=$request->file('avatar');
            if($avatar->isValid()){
                $name=md5(microtime(true)).'.'.$avatar->extension();
                $avatar->move('static/upload',$name);
                $path='/static/upload/'.$name;
                return '<a href=" '.$path.' ">查看文件</a>';
            }
            return $avatar-getErrorMessage();
        }
        return '文件上传失败';
    }
    public function user(){
        $data = Member::paginate(2);
        return view('user', compact('data'));
    }
    public function captcha()
    {
        return view('captcha');
    }
    public function checkCaptcha(Request $request)
    {
        $this->validate($request, [
            'captcha' => 'required|captcha'
        ],[
            'captcha.captcha' => '验证码有误'
        ]);
        return '验证成功';
    // return redirect('test/home');
    // return redirect('test/edit')->withErrors('home');
    }
    public function cache()
    {
        // $value=Cache::put('name','张三');
        // Cache::put('name',"张三");
        // Cache::put('name1',"李四",60);
        // dump($value);
        
        // Cache::add('age',60);
        // Cache::add('age',20);

    //     if (Cache::get('age')){
    //         dump('存在');
    //     }else{
    //         dump('不存在');
    //     }
    //    $value=Cache::get('name');
    //     dump($value);
    // 删除
    // Cache::pull('name');
    // Cache::forget('age');
    // Cache::flush();
    Cache::add('count',0,100);
    // Cache::increment('count');
    Cache::decrement('count');
    return '您是第'.Cache::get('count').'位访客';
    }
    public function testArr()
    {
        // $array=Arr::add(['name'=>'Tom'],'age','18');
        // dump(Arr::get($array,'name'));
        // dump(Arr::get($array,'gender','male'));
        // $array=Arr::except($array,['name']);
        // dump(Arr::get($array,'name'));

        // $array=['Desk','Table','Chair'];
        // $sorted=Arr::sort($array);
        // dump($sorted);
        // $num=[100,300,200];
        // $numsorted=Arr::sort($num);
        // dump($numsorted);

        // $array=[
        //     ['Roman','Taylor','Li'],
        //     ['PHP','React','JavaScript'],
        //     ['one'=>1,'two'=>2,'three'=>3],
        // ];
        // $sorted=Arr::sortRecursive($array);
        // dump($sorted);

    }
    public function testStr()
    {
      dump(Str::camel('foot_bar'));
      dump(Str::kebab('footBar'));
      dump(Str::snake('fooBar'));
      $matches=Str::is('foo*','footbar');
      $matchess=Str::is('foo*','barbar');
      dump($matches);
      dump($matchess);
    }
    public function testURL()
    {
        $url=action('TestController@testStr');
        $url1=action('TestController@form',['id'=>1]);
        // $url2=route('hello');
        $url3=asset('img/photo');
        dump($url);
        dump($url1);
        // dump($url2);
        dump($url3);

        $path=app_path();
        dump($path);
        dump(base_path());
    }
    public function xmldata()
    {
        $xml= new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>
        <booklist></booklist>');
        $book=$xml->addChild('book');
        $book->addChild('name','Laravel框架');
        $book->addChild('author','黑马程序员');
        // $book=$xml->addChild('book');
        // $book->addChild('name','vue.js的程序开发');
        // $book->addChild('author','黑马程序员');
        $content=$xml->asXML();
        return response($content)->header('Content-Type','text/xml');
    }
    public function xml(){
        return view('xml');
    }
    public function jsondata()
    {
        $data = [['name'=>'Tom','age'=>24],['name'=>'拜登','age'=>'81']];
        return response()->json($data);
    }
    public function json()
    {
        return view('json');
    }
   
}
