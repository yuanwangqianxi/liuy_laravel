<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    //
    protected $table = 'keyword';

    protected $timestamp = false;

    public function article()
    {
        return $this->belongsToMany(
            'App\Article', 
            'article_keyword',
            'keyword_id',
            'article_id'
        );
    }
}
