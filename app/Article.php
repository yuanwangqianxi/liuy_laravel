<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table='article';
    public $timestamps=false;
    // public function author()
    // {
    //     // return $this->hasOne('App\Author','id','author_id');
    //     return $this->belongsTo('App\Author','author_id', 'id');
    // }
    public function keyword()
    {
        return $this->belongsToMany(
            'App\Keyword',
            'article_keyword',
            'article_id', 
            'keyword_id'
        );
    }
}
